This dir contains a script that checks the results of the HammerCloud Singularity tests (all tests are available [here](https://github.com/CMSCompOps/MonitoringScripts/blob/420b4b70053d3480ad2cb399b7446bb81cf20685/hammercloud/eval_hc.py)). The HC tests run CRAB jobs, Montecarlo jobs, on 5 different sites.

The script evaluates the status of those jobs and alert in case in the last three days:
1. the failure rate of the jobs is higher than 10% at one site
2. there is a site where less than 50 jobs have been executed

The plan is also to send alerts when the singularity images available in cvmfs are not in synch with the images used by the ITB and production pools.

File list:

# eval\_hc.py

Library developed for the Site Support team. We need a function to evaluate the state of HC jobs by looking at the jobs in graphana. The version we use is [here](https://github.com/CMSCompOps/MonitoringScripts/blob/420b4b70053d3480ad2cb399b7446bb81cf20685/hammercloud/eval_hc.py) where this [pull request](https://github.com/CMSCompOps/MonitoringScripts/pull/99) has been applied.

# check\_singularity\_tests.py

