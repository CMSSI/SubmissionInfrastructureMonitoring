#!/usr/bin/python3

import os
import pwd
import sys
import time
import yaml
import pprint
import smtplib
import logging
import argparse
import urllib.request

from datetime import datetime
from collections import Counter
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import eval_hc

# Parameters for check_hc_jobs
NUM_DAYS_HC_HISTORY = 3
MIN_JOBS_SITE = 50
SUCCESS_RATE = .90

# Parameters for check_image_dates
NUM_DAYS_PROD_ALERT = 14
NUM_DAYS_ITB_ALERT = 7


#https://monit-grafana.cern.ch/d/cmsTMGlobal/cms-tasks-monitoring-globalview?orgId=11&var-user=All&var-site=All&var-current_url=%2Fd%2FcmsTMDetail%2Fcms_task_monitoring&var-task=All&var-Filters=data.CRAB_Workflow%7C%3D~%7C.*HC-202.*
#https://github.com/cms-sw/cms-docker/blob/master/cms/tags.yaml
#/cvmfs/singularity.opensciencegrid.org/cmssw
#https://github.com/CMSCompOps/MonitoringScripts/pull/99

class ProgramOutputHandler:
    output = ""
    error_thrown = False

    def print(self, msg=""):
        self.output += msg + "<br>"
        print(msg)

    def handle_err(self, msg=""):
        self.output += "<b>" + msg + "</b><br>"
        self.error_thrown = True
        print("\033[91m%s\033[0m" % msg)

    def send_mail(self, destinations):
        # Send email
        myacnt = pwd.getpwuid(os.getuid()).pw_name
        my_date = time.strftime("%Y-%b-%d %H:%M:%S UTC", time.gmtime())
        string = '<html><head></head><body><p>%s</p><p><i>%s: Alarm generated with the <a href="https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/-/blob/master/mmascher/check_singularity_tests.py">check_singularity_tests</a> of the CMSSI SubmissionInfrastructureMonitoring gitlab repo. Running at vocms0851.cern.ch</i></p></body></html>' % (self.output, my_date)

        msg = MIMEMultipart()
        msg['Subject'] = "SI Singularity image alarm"
#        msg['From'] = "saqib.haleem@cern.ch, marco.mascheroni@cern.ch"
#        msg['From'] = "cms-submission-infrastructure@cern.ch"
        msg['To'] = destinations
        msg.attach(MIMEText(string, 'html'))
        smtp_connection = smtplib.SMTP('localhost')
        smtp_connection.send_message(msg)
        smtp_connection.quit()


def check_image_dates(poh):
    FMT='%Y%m%d'
    today = time.strftime("%Y%m%d", time.gmtime())

    # Only consider images whose filename is like cms:rhel6-m20200904 (using string length to check this out)
    cvmfs_images = [ (x[4:9], x[-8:]) for x in os.listdir('/cvmfs/singularity.opensciencegrid.org/cmssw') if len(x)==len('cms:rhelV-mYYYYMMDD')]
    # Also consider images whose filename is like cms:rhel8-m-m20200904 (using string length to check this out)
    cvmfs_images += [ (x[4:11], x[-8:]) for x in os.listdir('/cvmfs/singularity.opensciencegrid.org/cmssw') if len(x)==len('cms:rhelV-m-mYYYYMMDD')]
    # Relying on the fact that the dict funtion will put the last item of the list in the dict if you have more than one value for the same key
    # E.g.: if the images list has [('rhel6', '20200605'), ('rhel6', '20200612'), ('rhel6', '20200702')] then the key 'rhel6' will have '20200702' as a value
    cvmfs_images = dict(sorted(cvmfs_images))

    poh.print("Checking images in CVMFS:")
    poh.print("    Latest images found in /cvmfs/singularity.opensciencegrid.org/cmssw:")
    poh.print("      " + pprint.pformat(cvmfs_images, indent=4))
    for imgos, imgdate in cvmfs_images.items():
        tdelta = datetime.strptime(today, FMT) - datetime.strptime(imgdate, FMT)
        poh.print("    CVMFS image for %s is %s days old" % (imgos, tdelta.days))

    poh.print()
    poh.print("Checking images used by the frontend:")
    with urllib.request.urlopen('https://raw.githubusercontent.com/cms-sw/cms-docker/master/cms/tags.yaml') as fdesc:
        frontend_images = yaml.load(fdesc)
    frontend_images = {k: v[-8:] for k,v in frontend_images.items()}
    poh.print("    Images found at https://github.com/cms-sw/cms-docker/blob/master/cms/tags.yaml:")
    poh.print("      " + pprint.pformat(frontend_images, indent=6))

    # Print some useful information about how old the frontend images are
    for imgos, imgdate in frontend_images.items():
        cvmfs_img = imgos if '-itb' not in imgos else imgos[:-4]
        if frontend_images[imgos] == cvmfs_images[cvmfs_img]:
            poh.print("    Frontend image for %s is synched" % imgos)
        else:
            tdelta = datetime.strptime(today, FMT) - datetime.strptime(imgdate, FMT)
            poh.print("    Frontend image for %s is not synched and it is %s days old" % (imgos, tdelta.days))

    # For each image check if what we have in cvmfs and in the production frontend is the same
    for imgos, imgdate in cvmfs_images.items():
        if imgos == 'rhel8': continue
        tdelta = datetime.strptime(today, FMT) - datetime.strptime(imgdate, FMT)
        if frontend_images[imgos] != cvmfs_images[imgos]:
            # if it is different and the latest cvmfs image is older than two weeks send an email
            if tdelta.days > NUM_DAYS_PROD_ALERT:
                poh.handle_err("The production frontend is currently using %s for architecture %s, but %s is available in CVMFS. Triggering this alarm since the CVMFS image is %s days old (>%s)" % (frontend_images[imgos], imgos, imgdate, tdelta.days, NUM_DAYS_PROD_ALERT))
        import pdb;pdb.set_trace()
        if frontend_images[imgos+"-itb"] != cvmfs_images[imgos]:
            if tdelta.days > NUM_DAYS_ITB_ALERT:
                poh.handle_err("The ITB frontend is currently using %s for architecture %s, but %s is available in CVMFS. Triggering this alarm since the CVMFS image is %s days old (>%s)" % (frontend_images[imgos], imgos, imgdate, tdelta.days, NUM_DAYS_ITB_ALERT))
    poh.print()


def check_hc_jobs(poh):
    now = int(time.time())

    eval_hc.evhc_glbl_cmssites = [ 'T1_DE_KIT', 'T1_US_FNAL', 'T1_IT_CNAF', 'T2_DE_DESY', 'T2_US_Caltech' ]
    eval_hc.evhc_glbl_templates = { '202' : {} }
    clause = [ {'regexp' : { 'data.CRAB_Workflow' : { "value" : ".*HC-202.*"}} } ]
    eval_hc.evhc_grafana_jobs(now - NUM_DAYS_HC_HISTORY*24*3600, now, clause) # Last two days

    res = {}
#    tasks = set()
    for item in eval_hc.evhc_glbl_jobcondor:
        res.setdefault(item['site'], []).append(item['status'])
#        tasks.add(item['refid'].split(' ')[1])

    for site in eval_hc.evhc_glbl_cmssites:
        if site not in res:
            poh.handle_err("No jobs found at %s" % site)
            continue
        d = Counter(res[site])
        poh.print(site + "\t" + str(d))
        total_jobs = sum(d.values())
        success_rate = d['Success'] / total_jobs
        if total_jobs < MIN_JOBS_SITE:
            poh.handle_err("Not enough jobs run at %s" % site)
        if success_rate < SUCCESS_RATE:
            poh.handle_err("Failure rate is too high at %s (%s)" % (site, 1 - success_rate))
            poh.print("CRAB Task output available at: https://monit-grafana.cern.ch/d/cmsTMGlobal/cms-tasks-monitoring-globalview?orgId=11&from=now-2d&to=now&var-user=sciaba&var-site=All&var-current_url=%2Fd%2FcmsTMDetail%2Fcms_task_monitoring&var-task=All&var-Filters=data.CRAB_Workflow%7C%3D~%7C.*HC-202-" + site + ".*")


def parse_arguments():
    parser = argparse.ArgumentParser(description='Check dates of singularity images and verify HC jobs state')
    parser.add_argument('--sendmail', action='store', default="",
                        help='To send an email in case of errors')

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = parse_arguments()
    poh = ProgramOutputHandler()
    logging.getLogger().setLevel(logging.DEBUG)
    check_image_dates(poh)
    check_hc_jobs(poh)
    if poh.error_thrown and args.sendmail:
        poh.send_mail(args.sendmail)
