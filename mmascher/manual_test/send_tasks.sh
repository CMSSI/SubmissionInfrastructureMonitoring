#!/bin/sh
alias cmsrel='scramv1 project CMSSW'
alias cmsenv='eval `scramv1 runtime -sh`'

# Check if the CMSSW releases are there, otherwise create them
[[ -d CMSSW_9_4_21_patch1 ]] || SCRAM_ARCH=slc6_amd64_gcc630 cmsrel CMSSW_9_4_21_patch1
[[ -d CMSSW_10_6_15 ]] || SCRAM_ARCH=slc7_amd64_gcc700 cmsrel CMSSW_10_6_39
[[ -d CMSSW_12_6_5 ]] || SCRAM_ARCH=el8_amd64_gcc10 cmsrel CMSSW_12_6_5
[[ -d CMSSW_13_2_10 ]] || SCRAM_ARCH=el9_amd64_gcc12 cmsrel CMSSW_13_2_10

for CMSSW_RELEASE in {CMSSW_9_4_21_patch1,CMSSW_10_6_39,CMSSW_12_6_5,CMSSW_13_2_10}; do
  cd $CMSSW_RELEASE
  cmsenv
  cd ..
  for SITE in {'T1_DE_KIT','T1_US_FNAL','T1_IT_CNAF','T2_DE_DESY','T2_US_Caltech'}; do
    export SITE
    crab-prod submit crabTask.py
  done
done

echo
echo
echo "Please, check this link to monitor the progresses of the tasks: https://monit-grafana.cern.ch/d/cmsTMGlobal/cms-tasks-monitoring-globalview?orgId=11&var-user=`whoami`&var-site=All&var-current_url=%2Fd%2FcmsTMDetail%2Fcms_task_monitoring&var-task=All"
