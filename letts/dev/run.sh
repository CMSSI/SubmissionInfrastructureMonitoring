#!/bin/sh
cd /home/letts/dev
# itb pool
FILE="/var/www/html/letts/itb.html"
./GetClassAds.py vocms0809.cern.ch
./AnalyzeJson.py > $FILE
rm HTCondorClassAds.*.js
# production pools
FILE="/var/www/html/letts/production.html"
cd /home/letts/new
./GetClassAds.py
./AnalyzeJson.py > $FILE
rm HTCondorClassAds.*.js
exit