import os
import htcondor
import classad
import json
import time
from my_utils import convert_ClassAd_to_json
import logging 
import pdb
import sys
import socket
import argparse
import yaml

# CMS modules
from CMSMonitoring.StompAMQ7 import StompAMQ7

''' 
    Reads the  attributes in <filename> and creates a list
    :param filename: path to file with a list of classAd attributes
    :return: a list of classAd attributes to be used as projection
'''
def get_projection_from_file(filename):
    attr_list=[]
    fd = open(filename, 'r')
    for attr in fd.readlines():
        attr_list.append(attr[:-1])    
    return attr_list

def convert_ads_to_dict_list(ads):
    # TODO: make convertion to dictionary directly
    dict_list=[]
    for ad in ads:
        #TODO:
            # - fix convert_ClassAd_to_json to not append last comma
        json_ad=convert_ClassAd_to_json(ad)
        #dict_ad=json.loads(json_ad[:-1])
        dict_ad=json.loads(ad.printJson())
        dict_list.append(dict_ad)

    return dict_list

'''
   Evaluate classAd attributes that are returned as expressions form condor
   :param ads: list of ClassAds that contain expressions within its attributes
   :param list_of_attrs: list of attributes that are returned as expressions
    from condor

'''
def evaluate_classad_expressions(ads, list_of_attributes_to_evaluate):
    for ad in ads:
        for attr in list_of_attributes_to_evaluate:
            try:
                if type(ad[attr]) == classad.classad.ExprTree:
                    ad[attr]=classad.ExprTree.eval(ad[attr])
            except KeyError :
                log.error("Cannot find key: %s for site %s",attr , ad['GLIDEIN_CMSSite'])
                continue

'''
   Creates schedd objects from <schedd_ads> and queries them to get the number
   of autoclusters in idle jobs, then it adds the calculated value into each
   of the schedd ads in <schedd_ads>.
   :param schedd_ads: the list of classAds of the schedds to be queried
'''
def add_idle_autoclusters(schedd_ads):
    for schedd_ad in schedd_ads:
        autocluster_ids_set=set([])
        schedd = htcondor.Schedd(schedd_ad)
        try:
            job_ads=schedd.query("AutoclusterId=!=UNDEFINED && jobStatus==1", ['AutoclusterId'])
        except RuntimeError :
            log.error("Connecting to schedd: %s", schedd_ad['Name'])
            continue
        except:
            e = sys.exc_info()
            log.error("Unexpected exception: %s", str(e))
            exit(1)
    else:
        for job_ad in job_ads:
            autocluster_ids_set.add(job_ad['AutoclusterId'])
        autoclusters_ids_len=len(autocluster_ids_set)
        schedd_ad.update({"IdleAutoclusters":autoclusters_ids_len})

def trim_classAd(ads, extra_projection):
    for ad in ads:
        for attribute in extra_projection:
            del ad[attribute]

'''
    Queries the <collector> for ads of the type <condor_ad_type> using
    the projection in <projection> and push them to the AMQ service
    instance represented by <amq>
    :param collector: htcondor collector object to be queried
    :param condor_ad_type: the type of ClassAd to fetch e.g. htcondor.AdTypes.Schedd
    :param projection: list of attributes to be queried
    :param amq: the StompAMQ instance to be used to push the ads
    :param ad_type: the "type" parameter in the metric e.g "schedd"
    :param pool: the pool name to be used in the metadata of the metric e.g "itb"
    :param constraint: the constraint used to query the condor daemon
    :param output_action: push: push the ads, print: print the ads, both: push and print the ads
    :param list_of_attributes_to_evaluate: a subset of <projection>, these attributes need to
    are represented as expression and need to be evaluated
    :param constraint: a constraint to be passed to the condor query
    :param extra_projection: a set of attributes that are required for internal processing but
    :param producer: the data producer  (cms in production, cms-test in test)
    are not being sent to the monitoring infrastructure so need to be trimmed
'''
def pull_and_push(collector, condor_ad_type, projection, amq, ad_type, pool, output_action, list_of_attributes_to_evaluate, constraint="true", extra_projection=[], producer="cms"):
    # Timestamp recorded before calling collector query
    # Time in miliseconds is required.
    timestamp=int(time.time()*1000)
    ads = collector.query(condor_ad_type, constraint, projection)

    # some attributes come as expressions and need to be evaluated
    if(len(list_of_attributes_to_evaluate) != 0):
        evaluate_classad_expressions(ads, list_of_attributes_to_evaluate)

    # For the schedds, we need an extra attribute to denote the number of
    # Autoclusters in the schedd which belongs to idle jobs
    # We avoid doing this query on "cern" pool becuase it duplicates
    # the information we already have from the "global" pool. The
    # schedds in the "cern" pool are also in the "global" pool due to flocking

    # Disabling temporariy the query for idle autocluster
    # https://cms-logbook.cern.ch/elog/GlideInWMS/6744
    #if(condor_ad_type == htcondor.AdTypes.Schedd and pool != "cern"):
    #    add_idle_autoclusters(ads)

    # If <projection> has extra attributes(only needed for internal processing)
    # they need to be trimmed
    if(len(extra_projection) != 0):
        trim_classAd(ads, extra_projection)

    # Convert classAd format into a dictionary list
    dict_list=convert_ads_to_dict_list(ads)
    metadata={
              'type_prefix' : "raw",
              'version' : "0.2",
              'pool' : pool}

    doc_type = 'si_condor_' + ad_type
    notifications = []
    for payload in dict_list:
        ndoc, _, _ = amq.make_notification(payload, doc_type, data_subfield='payload', producer=producer, ts=timestamp, metadata=metadata)
        notifications.append(ndoc)
    # Print the collected data
    if output_action == "both" or output_action == "print":
        for notification in notifications:
            print(json.dumps(notification, sort_keys=True, indent=4))
    # Push the collected data
    if output_action == "both" or output_action == "push":
        amq.send(notifications)

def pull_and_push_autoclusters(collector, projection, amq, ad_type, pool, output_action, constraint="true", producer="cms"):
    projection_aux=['Machine','CondorPlatform', 'Name', 'AddressV1', 'MyAddress', 'CondorVersion']
    schedd_ads =  collector.query(htcondor.AdTypes.Schedd, "true", projection_aux)
    # Time in miliseconds is required
    timestamp=int(time.time()*1000)
    for schedd_ad in schedd_ads:
        schedd_name=schedd_ad['name']
        schedd = htcondor.Schedd(schedd_ad)
        try:
            #TODO:
                # - why it doesn't work with query()
                # - do we want all the autoclusters or only those idle?
            ads=schedd.xquery(constraint, opts=htcondor.QueryOpts.AutoCluster)
        except RuntimeError :
            log.error("Connecting to schedd: %s", schedd_ad['Name'])
            continue
        except:
            e = sys.exc_info()
            log.error("Unexpected exception: %s", str(e))
            exit(1)
        else:
            dict_list=convert_ads_to_dict_list(ads)
            metadata={
              'type_prefix' : "raw",
              'version' : "0.2",
              'pool' : pool,
              'schedd' : schedd_name}

            doc_type = 'si_condor_'+ad_type
            notifications = []
            for payload in dict_list:
                ndoc, _, _ = amq.make_notification(payload, doc_type, data_subfield='payload', producer=producer, ts=timestamp, metadata=metadata)
                notifications.append(ndoc)
            # Print the collected data
            if output_action == "both" or output_action == "print":
                for notification in notifications:
                    print(json.dumps(notification, sort_keys=True, indent=4))
            # Push the collected data
            if output_action == "both" or output_action == "push":
                amq.send(notifications)


'''
    Pick from a list of HA collectors, the one running the negotiator(s) daemons
    :param list_of_collectors : the list of HA collectors
    :return: one item from the <list_of_collectors>
'''
def get_main_collector(list_of_collectors):

    # If the list is empty, exit
    if len(list_of_collectors) == 0:
        log.error("List of collectors is empty")
        exit(6)

    main_collector=""
    for collector_name in list_of_collectors:
        collector = htcondor.Collector(collector_name)
    try:
        ads=collector.query(htcondor.AdTypes.Negotiator, "true", ["Name"])
    except:
        log.info("negotiator(s) not running in the primary collector")
    else:
        if len(ads) !=0:
            if main_collector != "":
                # more than one collector running negotiator(s)
                log.warning("Negotiators are running in both HA hosts")
            main_collector=collector_name

    # if no negotiator running anywhere, pick the first collector
    if main_collector == "":
        log.warning("Cannot find any negotiator running")
        main_collector=list_of_collectors[0]

    return main_collector


###############################################################################
#                                                                             #
#                               MAIN                                          #
#                                                                             #
###############################################################################

#-----------------------------------------------------------------------------#
# Set basic configuration
#-----------------------------------------------------------------------------#

# Setup the logger and the log level {CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET}
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s', datefmt='%d-%m-%y %H:%M:%S')
log = logging.getLogger(__name__)

parser = argparse.ArgumentParser(
    prog = 'CMS SI Monitoring',
    description = 'Monitor the CMS Submission Infrastructure')
parser.add_argument('-c', '--config', dest='config', action='store', type=str, default="/etc/condormon.yaml")
parser.add_argument('pool', )
parser.add_argument('action', choices=['push', 'print', 'both'])
args = parser.parse_args()

config = yaml.safe_load(open(args.config))

pool_name=args.pool
output_action=args.action

#-----------------------------------------------------------------------------#

# Is pool in pool map?
if not pool_name in config['pool_collector_map']:
    log.error("Incorrect pool name, expecting: %s and got instead: %s", str(config['pool_collector_map'].keys()), sys.argv[1])
    exit(3)

# Get a list of collector names from the pool, using the <pool_collector_map>
list_of_collectors=config['pool_collector_map'][pool_name]

# Pick the main collector (the one running the
collector_name=get_main_collector(list_of_collectors) 

log.info("\n Pool name: %s\n Output action: %s\n Collector name: %s\n", pool_name, output_action, collector_name)

#-----------------------------------------------------------------------------#
# Read projection list from files
#-----------------------------------------------------------------------------#

projection_schedd=get_projection_from_file("classAds/schedd")
extra_projection_schedd=['Machine','CondorPlatform', 'AddressV1', 'MyAddress', 'CondorVersion']

# Adding few attributes that are needed in order to build a schedd object to be
# queried. The attributes at <extra_projection_attributes> will NOT be pushed
# into the monit infrastructure
#TODO:
#    - make sure attributes are not repeated between projection_schedd and
projection_schedd+=extra_projection_schedd

projection_startd=get_projection_from_file("classAds/startd")
projection_negotiator=get_projection_from_file("classAds/negotiator")
projection_collector=get_projection_from_file("classAds/collector")
projection_autocluster=get_projection_from_file("classAds/autocluster")
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
# Create collector and amq objects, to pull and push data
# respectively
#-----------------------------------------------------------------------------#

collector = htcondor.Collector(collector_name)
collector9620 = htcondor.Collector(collector_name+":9620")
# Secondary Collector name, in case of more than one collectors
# By Default, List contains secondary collector in last
collector_secondary = htcondor.Collector(list_of_collectors[-1])

amq=StompAMQ7(config['monit_username'], config['monit_password'], config['monit_producer'], config['monit_topic'], validation_schema=None, host_and_ports=[(config['monit_hostname'], config['monit_port'])], logger=log)
#-----------------------------------------------------------------------------#

# Pull and Push data from Schedds
log.info("Pushing data from schedds")
list_of_attributes_to_evaluate=["CurbMatchMaking"]
pull_and_push(collector9620, htcondor.AdTypes.Schedd, projection_schedd, amq, "schedd", pool_name, output_action, list_of_attributes_to_evaluate, extra_projection=extra_projection_schedd, producer=config['monit_producer'])


list_of_attributes_to_evaluate=[]
# Pull and Push data from Startds
log.info("Pushing data from startds")
list_of_attributes_to_evaluate=["TotalMemoryUsage", "MemoryUsage"]

# DODAS and CINECA Startds (slots) are not reporting to FNAL backup collector.
# As, an exception pull only DODAS & CINECA slots info. from CERN Global collector
if pool_name == "global":

    CERN_GLOBAL_COLLECTOR = htcondor.Collector("vocms4100.cern.ch")
    startd_const='CMSSubsiteName == "CNAF-CINECA" || GLIDEIN_CMSSite == "T3_IT_Opportunistic_dodas"'
    log.info("Pushing data from Italian Dynamic and Opportunistic startds")
    list_of_attributes_to_evaluate=["TotalMemoryUsage", "MemoryUsage"]
    pull_and_push(CERN_GLOBAL_COLLECTOR, htcondor.AdTypes.Startd, projection_startd, amq, "startd", pool_name, output_action, list_of_attributes_to_evaluate, constraint=startd_const, producer=config['monit_producer'])
    pull_and_push(collector_secondary, htcondor.AdTypes.Startd, projection_startd, amq, "startd", pool_name, output_action, list_of_attributes_to_evaluate, producer=config['monit_producer'])
else:
    pull_and_push(collector, htcondor.AdTypes.Startd, projection_startd, amq, "startd", pool_name, output_action, list_of_attributes_to_evaluate, producer=config['monit_producer'])
#################################################################################

# Pull and Push data from Collector (only from main collector and backup, not ccb)
# For the constraint we should always use the hostname not the alias
# Collect data of Negotiator and Collector daemon(s) data from both Primary and Backup CM


for coll in list_of_collectors:
      collector_hostname=socket.gethostbyaddr(coll)[0]
      try:
        pool_collector=htcondor.Collector(collector_hostname)
        list_of_attributes_to_evaluate=[]
        if coll == "cmsgwms-collector-itb.cern.ch":
            const= 'Machine == "'+ str(collector_hostname) +'" && Name == "ITB Top Level COllector at  vocms0808.cern.ch@vocms0808.cern.ch"'
        elif coll == "cmsgwms-collector-itb.fnal.gov":
            const= 'Machine == "'+ str(collector_hostname) +'" && Name == "itb_central_manager@cmssrv623.fnal.gov"'
        else:
            const='Machine == "'+ str(collector_hostname) +'"'
        log.info("Pushing data from collector")
        log.info("\n Pool collector: %s\n constraint: %s\n", collector_hostname , const)
        pull_and_push(pool_collector, htcondor.AdTypes.Collector, projection_collector, amq, "collector", pool_name, output_action, list_of_attributes_to_evaluate,constraint=const,producer=config['monit_producer'])
        log.info("Pushing data from negotiator(s) %s", collector_hostname)
        pull_and_push(pool_collector, htcondor.AdTypes.Negotiator, projection_negotiator, amq, "negotiator", pool_name, output_action, list_of_attributes_to_evaluate, producer=config['monit_producer'])
      except RuntimeError:
        log.error("Connecting to collector: %s", collector_hostname)
        continue
################################################################################
# Pull and Push data from Autoclusters
log.info("Pushing data from autoclusters")
const='JobStatus == 1'
pull_and_push_autoclusters(collector9620, projection_autocluster, amq, "autocluster", pool_name, output_action, constraint=const, producer=config['monit_producer'])

###############################################################################

