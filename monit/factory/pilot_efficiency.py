"""
This module collects data from condor activity logs and sends it to MONIT
"""
import datetime
import glob
import json
import re
import sys
import os
import time
import traceback
from optparse import OptionParser
import logging
from logging import handlers
import configparser
import pickle
import pytz
from CMSMonitoring.StompAMQ7 import StompAMQ7


def get_amq(host, port, producer, topic, ckey, cert):
    """
    Create StompAMQ object
    """
    username = ""
    password = ""
    amq = StompAMQ7(username,
                    password,
                    producer,
                    topic,
                    key=ckey,
                    cert=cert,
                    validation_schema=None,
                    host_and_ports=[(host, port)])

    return amq


def create_documents(directory):
    """
    Create documents
    """
    documents = []
    files = glob.glob(f"{directory}*.*.*.*.*.*")
    time_from = datetime.datetime.now() - datetime.timedelta(hours=2)
    time_from = time_from.replace(minute=30, second=0, microsecond=0, tzinfo=pytz.timezone("Europe/Paris"))
    time_to = time_from + datetime.timedelta(hours=1)
    for f in files:
        factory, site, entry, frontend, frontend_group, cores = re.search(".*\/([^.]+).([^.]+).([^.]+).([^.]+).([^.]+).([^.]+)", f).groups()
        with open(f, "rb") as fd:
            data = pickle.load(fd)
        for pilot_id, pilot_values in data.items():
            if not ("terminated" in pilot_values and "executing" in pilot_values):
                continue

            if factory.startswith("CERN"):
                time_zone = "Europe/Paris"
            elif factory.startswith("FNAL"):
                time_zone = "America/Chicago"
            elif factory.startswith("UCSD"):
                time_zone = "America/Los_Angeles"
            else:
                continue

            end_time = datetime.datetime.fromtimestamp(float(pilot_values["terminated"]))
            end_time = end_time.replace(tzinfo=pytz.timezone(time_zone))
            if not time_from <= end_time < time_to:
                continue

            start_time = datetime.datetime.fromtimestamp(float(pilot_values["executing"]))
            start_time = start_time.replace(tzinfo=pytz.timezone(time_zone))
            documents.append({
                "factory": factory,
                "entry": entry,
                "frontend": frontend,
                "frontend_group": frontend_group,
                "pilot": pilot_id,
                "start_time": datetime.datetime.timestamp(start_time),
                "end_time": pilot_values["terminated"],
                "walltime": (end_time - start_time).total_seconds(),
                "norm_sys_usage": pilot_values["norm_sys_usage"],
                "norm_usr_usage": pilot_values["norm_usr_usage"],
                "site": site,
                "cores": cores,
            })

    return documents


def create_notification_documents(amq, documents, document_type):
    """
    Create notification documents
    """
    seconds = str(int(time.time()))
    data = []
    for document in documents:
        factory = document.get("factory")
        document_id = "%s_%s_%s_%s_%s_%s" % (factory,
                                             document["entry"],
                                             document["pilot"],
                                             document["frontend"],
                                             document["frontend_group"],
                                             seconds)
        notification, _, _ = amq.make_notification(document,
                                                   document_type,
                                                   doc_id=document_id,
                                                   metadata={"factory": factory,
                                                             "doc_type": document_type},
                                                   data_subfield=None)
        data.append(notification)

    return data


def setup_logging():
    """
    Setup the logger
    """
    main_logger = logging.getLogger("logger")
    main_logger.setLevel(logging.INFO)
    log_format = "[%(asctime)s][%(levelname)s] %(message)s"
    formatter = logging.Formatter(fmt=log_format, datefmt="%Y-%m-%d %H:%M:%S")
    console = logging.StreamHandler()
    main_logger.addHandler(console)
    main_logger.propagate = False
    console.setFormatter(formatter)

    sys.stdout = LoggerWriter(main_logger.info)
    sys.stderr = LoggerWriter(main_logger.error)

    return main_logger


class LoggerWriter(object):
    """
    Wrapper class to redirect write function to a given function
    """
    def __init__(self, level):
        self.level = level

    def write(self, message):
        """
        If message is not only newline,
        rstrip and pass it to given function
        """
        if message != "\n":
            self.level(message.rstrip())

    def flush(self):
        pass


def add_file_logging(log_file_name):
    """
    Log to file
    """
    # Max log file size - 10Mb
    max_log_file_size = 1024 * 1024 * 10
    max_log_file_count = 10
    log_directory = "/".join(log_file_name.split("/")[:-1])
    if log_directory and not os.path.exists(log_directory):
        os.makedirs(log_directory)

    handler = handlers.RotatingFileHandler(log_file_name,
                                           "a",
                                           max_log_file_size,
                                           max_log_file_count)

    formatter = logger.handlers[0].formatter
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def main():
    """
    Collect data of pilot efficiency and send it to MONIT
    """
    parser = OptionParser()
    parser.add_option("--send",
                      action="store_true",
                      default=False,
                      dest="send",
                      help="If present, send created documents to selected destination. "
                           "If absent, print created documents and do not send them to "
                           "selected destination.")
    parser.add_option("--log_to_file",
                      default="",
                      dest="log_to_file",
                      help="Print logs to file")
    destination_choices = ("training", "testing", "production")
    parser.add_option("--destination",
                      type="choice",
                      choices=destination_choices,
                      default=destination_choices[0],
                      dest="destination",
                      help="Send created documents to selected destination. Valid choices are %s. "
                           "Default: %s" % (destination_choices, destination_choices[0]))
    parser.add_option("--config",
                      default="",
                      dest="config_name",
                      help="Get information from configuration file")
    (options, _) = parser.parse_args()
    log_file_name = options.log_to_file
    if log_file_name:
        add_file_logging(log_file_name)

    logger.info("Started")
    config_name = options.config_name
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.optionxform = str
    if os.path.isfile(config_name):
        config.read(config_name)
    else:
        logger.error(config_name + " file does not exist")
        return
    destination = options.destination
    logger.info("Selected destination is %s", destination)
    try:
        producer = config.get(destination, "producer")
        topic = config.get(destination, "topic")
        global ckey
        global cert
        ckey = config.get(destination, "ckey")
        cert = config.get(destination, "cert")
        document_type_for_pilot_efficiency = config.get(destination,
                                                        "document_type_for_pilot_efficiency")
        host = str(config.get(destination, "host"))
        port = str(config.get(destination, "port"))
        pilot_efficiency_directory = config.get(destination, "pilot_efficiency_directory")
    except (configparser.NoSectionError, configparser.NoOptionError) as ex:
        logger.error(ex)
        return
    amq = get_amq(host, port, producer, topic, ckey, cert)
    documents = create_documents(pilot_efficiency_directory)
    notification_documents = create_notification_documents(amq,
                                                           documents,
                                                           document_type_for_pilot_efficiency)
    if not options.send:
        logger.info(json.dumps(notification_documents, indent=4))
    else:
        logger.info("Will send created documents to %s monitoring", destination)
        amq.send(notification_documents)
    logger.info("Finished")


if __name__ == "__main__":
    logger = setup_logging()
    try:
        main()
    except Exception as ex:
        logger.error(ex)
        logger.error(traceback.format_exc())
