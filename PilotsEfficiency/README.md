# Summary

The efficiency.py script analyzes activity logs from a factory and calculates the overall efficiency of pilots running on computing elements (CEs).

The script takes one argument, which is the path to a single activity log file. Here is an example of how to run the script:

```
python3 efficiency.py /var/log/gwms-factory/client/user_fecmsglobal/glidein_gfactory_instance/entry_CMSHTPC_T2_DE_DESY_grid-htcondorce0/condor_activity_20230504_CMSG-v1_0.main-token.log
Total walltime: 145 days and 7 hours with 100.00% termination rate (#jobs)
Total efficiency 84.11%

python3 efficiency.py /var/log/gwms-factory/client/user_fecmsglobal/glidein_gfactory_instance/entry_CMSHTPC_T2_DE_DESY_grid-htcondorce1/condor_activity_20230504_CMSG-v1_0.main-token.log
Total walltime: 157 days and 21 hours with 98.86% termination rate (#jobs)
Total efficiency 99.39
```

In the example above, the efficiency of pilots running on the "grid-htcondorce0" and "grid-htcondorce1" CEs at DESY are calculated. The script outputs the total walltime (the sum of the walltime of all pilots found in the activity log) and the efficiency of the pilots.

# Details

Activity logs contain Condor job events, and the script only considers jobs that have a "terminated" event when calculating the efficiency. The walltime is calculated by subtracting the timestamp of the executing event from the timestamp of the terminated event. The script then sums the Usr and Sys usage as reported in the terminated event and divides the resulting values by a constant number of 8 CPUs. The efficiency is the usage divided by the walltime.

The script outputs the total walltime and the efficiency to provide a sense of the amount of statistics considered. Sometimes, even if a pilot has an executing event, it does not have a terminated event (e.g., the pilot is evicted on the WN or removed by the factory). The percentage of terminated pilots over the executing ones is also printed to provide more information.



# Activity logs example

Here are the events logged into the activity log for one pilot:

```
000 (7714638.001.000) 2023-05-04 00:44:56 Job submitted from host: <188.184.99.97:9615?addrs=188.184.99.97-9615+[2001-1458-d00-3a--100-110]-9615&alias=vocms0207.cern.ch&noUDP&sock=scheddglideins3>
...
027 (7714638.001.000) 2023-05-04 00:45:10 Job submitted to grid resource
    GridResource: condor grid-htcondorce1.desy.de grid-htcondorce1.desy.de:9619
    GridJobId: condor grid-htcondorce1.desy.de grid-htcondorce1.desy.de:9619 5400384.0
...
001 (7714638.001.000) 2023-05-04 18:08:05 Job executing on host: condor grid-htcondorce1.desy.de grid-htcondorce1.desy.de:9619
...
005 (7714638.001.000) 2023-05-06 15:41:33 Job terminated.
(1) Normal termination (return value 0)
Usr             16 05:34:59, Sys 0 06:51:14  -  Run Remote Usage
Usr 0 00:00:00, Sys         0 00:00:00  -  Run Local Usage
Usr 16 05:34:59, Sys 0 06:51:14  -  TotemSDtal Remote Usage
Usr 0 00:00:00, Sys 0 00:00:00  -  Total Local Usage
0  -  Run Bytes Sent By Job
0  -  Run Bytes Received By Job
0  -  Theotal Bytes Sent By Job
0  -  Total Bytes Received By Job
```

# Running on multiple files

If you wish to run the script on multiple files and get the efficiency over multiple days or multiple CEs, you can use a file expression that the script will expand using the glob library. Here is an example:


```
python3 efficiency.py "/var/log/gwms-factory/client/user_fecmsglobal/glidein_gfactory_instance/entry_CMSHTPC_T2_DE_DESY_grid-htcondorce1/condor_activity_202305*_CMSG-v1_0.main-token.log"
Total walltime: 803 days and 16 hours with 68.75% termination rate (#jobs)
Total efficiency 97.20%
```

The command above calculates the efficiency for pilots running on all CEs at DESY in May 2023. The script outputs the total walltime and efficiency over all the activity logs that match the file expression. Pay attention since factory activity logs rotates. Make also sure to put the quotes around the file expression, otherise bash will expand it and the script will only analyze the first file (`argv[1]`).

# Conclusions

This script is useful for analyzing the efficiency of pilots running on CEs in a factory. It provides information on the walltime and usage of the pilots and calculates the overall efficiency.
