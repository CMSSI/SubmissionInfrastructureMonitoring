import re
import sys
import glob
import time
import datetime
#import json
import pickle

#https://monit-grafana.cern.ch/d/000000628/cms-job-monitoring-es-agg-data-official?orgId=11&var-group_by=CMS_JobType&var-Tier=All&var-CMS_WMTool=All&var-CMS_SubmissionTool=All&var-CMS_CampaignType=All&var-Site=T2_DE_DESY&var-Type=All&var-CMS_JobType=All&var-CMSPrimaryDataTier=All&var-binning=1h&viewPanel=53&from=1672531200000&to=1677628799000
#https://accounting.egi.eu/egi/site/DESY-HH/cpueff/VO/DATE/2023/1/2023/2/lhc/onlyinfrajobs/
#https://accounting.egi.eu/egi/site/DESY-HH/cpueff/SubmitHost/VO/2023/4/2023/4/lhc/onlyinfrajobs/
CPUS = 8
FILE_EXPR = "/var/log/gwms-factory/client/user_fecmsglobal/glidein_gfactory_instance/entry_CMSHTPC_T2_DE_DESY_grid-htcondorce1/condor_activity_*_CMSG-v1_0.main-token.log"


def parseline(line):
    jobid = line.split("(")[1].split(")")[0]
    sdate = line.split(") ")[1].split(" Job")[0]
    event_time = time.mktime(datetime.datetime.strptime(sdate,"%Y-%m-%d %H:%M:%S").timetuple())

    return jobid, event_time


def map_eventline(line):
    event_map = {
        "Job submitted to grid resource": "submitted",
        "Job executing on host" : "executing",
        "Job terminated": "terminated",
        "Job was aborted": "aborted",
        "Job was evicted": "evicted",
        "Job was held": "held",
    }

    for event in event_map:
        if event in line:
            return event_map[event]

    return None


def usage_to_seconds(usage):
    i_, day, rest = usage.split(" ")
    hours, minutes, seconds = rest.split(":")

    return 60*60*24*int(day) + 60*60*int(hours) + 60*int(minutes) + int(seconds)


def parse_file(event_file, jobs, cpus, gridtype):
    for line in event_file: # Should probably iterate over events (splitting file over "...") and not lines..
        event = map_eventline(line)
        if event:
            jobid, event_time = parseline(line)
            jobs.setdefault(jobid, {})[event] = event_time
            continue
        if "Run Remote Usage" in line:
            # Relying on last event line being correct. That's why we should iterate over events.
            susage = line.split("-")[0].strip()
            jobs[jobid]["susage"] = susage
            usr, sys = susage.split(", ")
            jobs[jobid]["norm_sys_usage"] = usage_to_seconds(sys)/(cpus*(60 if gridtype=="arc" else 1))
            jobs[jobid]["norm_usr_usage"] = usage_to_seconds(usr)/(cpus*(60 if gridtype=="arc" else 1))


class Print:
    def __init__(self, jobs):
        self.jobs = jobs

        badjobs = []
        for job, events in self.jobs.items():
            if "terminated" in events and "executing" not in events:
                   print(f"Job {job} has a terminated event but no executing. Removing it.", file=sys.stderr)
                   badjobs.append(job)
                   continue
        for job in badjobs:
            del jobs[job]

    def print_terminated(self):
        for job, events in self.jobs.items():
            if "terminated" in events:
                lasted = events["terminated"] - events["executing"]
                efficiency = (events["norm_sys_usage"] + events["norm_usr_usage"]) / lasted
                print(f"Job {job} lasted {lasted} and with normalized usr usage {events['norm_usr_usage']} and normalized sys usage {events['norm_sys_usage']}. Efficiency is {efficiency}")

    def print_overall_efficiency(self, filter_short=False, print_walltime=False):
        total_lasted = 0
        total_sys = 0
        total_usr = 0
        short_jobs = 0
        executing_jobs = 0
        terminated_jobs = 0

        for job, events in self.jobs.items():
            if "executing" in events:
               executing_jobs += 1
            if "terminated" in events:
               terminated_jobs += 1
               lasted = events["terminated"] - events["executing"]
               if filter_short and lasted < 1800:
                   short_jobs += 1
                   continue
               total_lasted += lasted
               total_sys += events["norm_sys_usage"]
               total_usr += events["norm_usr_usage"]

        if print_walltime is True:
            wall_msg = f"Total walltime: {int(total_lasted/(3600*24))} days and {int((total_lasted%(3600*24))/3600)} hours"
            wall_msg += f" with {terminated_jobs/executing_jobs:.2%} termination rate (#jobs)" if executing_jobs is not 0 else "" # Can't divide by 0..
            print(wall_msg)
        if total_lasted == 0:
            print("Not printing overall efficiency since we got no terminated jobs", file=sys.stderr)
            return
        print(f"Total efficiency {(total_sys+total_usr)/total_lasted:.2%}")
        if filter_short is True:
           print(f"filter_short was True. Found {short_jobs} terminated short jobs")

    def print_exit_status(self):
        print(f"Number of jobs {len(self.jobs)}")
        print("Jobs with executing event: {}".format(len([ x for x in self.jobs.values() if "executing" in x])))
        print("Good jobs with both executing and terminated event {}".format(len([ x for x in self.jobs.values() if "executing" in x and "terminated" in x])))
        print("This should be zero: {}".format(len([ x for x in self.jobs.values() if "executing" in x and "terminated" in x and ("evicted" in x or "aborted" in x)])))
        print("Jobs that got evicted: {}".format(len([ x for x in self.jobs.values() if "executing" in x and "evicted" in x])))
        print("Jobs held: {}".format(len([ x for x in self.jobs.values() if "executing" in x and "held" in x])))
        print("Jobs aborted: {}".format(len([ x for x in self.jobs.values() if "executing" in x and "aborted" in x])))
        print("Both aborted and evicted: {}".format(len([ x for x in self.jobs.values() if "executing" in x and "aborted" in x and "evicted" in x])))

        print("Id of jobs not terminated: {}".format([str(jobid) for jobid, events in self.jobs.items() if "executing" in events and "terminated" not in events]))


def old_main():
    if len(sys.argv) > 1:
        FILE_EXPR = sys.argv[1]
    file_list = sorted(glob.glob(FILE_EXPR))

    jobs = {}
    for event_file in file_list:
        with open(event_file) as event_file_desc:
            parse_file(event_file_desc, jobs, 8)

    for job in jobs:
        print(f"{job} {jobs[job]}")
#    p = Print(jobs)
#    p.print_terminated()
#    print()
#    p.print_exit_status()
#    print()
#    p.print_overall_efficiency()
#    print()
#    p.print_overall_efficiency(print_walltime=True)

def group_info(file_list):
    res = {}
    for event_file in file_list:
        factory, frontend, sdate, group = re.search('/data/glideinmonitor/rsync/([^/]+)/user_([^/]+)/glidein_gfactory_instance/entry_[^/]+/condor_activity_([^_]+)_[^.]+.([^.]+).log', event_file).groups()
        file_time = time.mktime(datetime.datetime.strptime(sdate,"%Y%m%d").timetuple())
        if (time.time() - file_time)/(60*60*24) > 10:
            continue
        res.setdefault((factory, frontend, group),[]).append(event_file)
    return res


def main():
    old_site = None
    jobs = {}
    with open("CMS_SITES") as cms_sites:
        for line in cms_sites:
            site, entry, cpus, gridtype = line.strip().split(", ")

            if cpus == "auto":
                print(f"Skipping wholenode entry {entry}", file=sys.stderr)
                continue

#            if old_site != site and jobs != {}: # New site with jobs
#                print(f"========== {old_site} ==========")
#                p = Print(jobs)
#                p.print_overall_efficiency(print_walltime=True)
#                jobs={}
#                pass

            file_list = sorted(glob.glob(f"/data/glideinmonitor/rsync/*/user_*/glidein_gfactory_instance/entry_{entry}/condor_activity_*.log"))
            res = group_info(file_list)

            for (factory, frontend, group), activityfile_list in res.items():
                jobs = {}
                for event_file in activityfile_list:
                    with open(event_file) as event_file_desc:
                        parse_file(event_file_desc, jobs, int(cpus), gridtype)
                with open(f"/data/glideinmonitor/tmp/{factory}.{site}.{entry}.{frontend}.{group}.{cpus}","wb") as fd:
                    pickle.dump(jobs, fd, pickle.HIGHEST_PROTOCOL)
             

#            for event_file in file_list:
#                factory, frontend, sdate, group = re.search('/data/glideinmonitor/rsync/([^/]+)/user_([^/]+)/glidein_gfactory_instance/entry_[^/]+/condor_activity_([^_]+)_[^.]+.([^.]+).log', event_file).groups()
#                file_time = time.mktime(datetime.datetime.strptime(sdate,"%Y%m%d").timetuple())
#                if (time.time() - file_time)/(60*60*24) > 10:
#                    continue
#                with open(event_file) as event_file_desc:
#                    parse_file(event_file_desc, jobs, int(cpus), gridtype)
#
#                with open(f"/data/glideinmonitor/tmp/{factory}.{site}.{entry}.{frontend}.{group}.{cpus}","w") as fd:
#                    json.dump(jobs, fd)
#            old_site = site

if __name__ == "__main__":
#    old_main()
    main()
